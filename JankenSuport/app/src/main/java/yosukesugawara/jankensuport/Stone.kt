package yosukesugawara.jankensuport

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_stone.*

class Stone : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stone)

        val back = findViewById<Button>(R.id.backButtom)
        val pon = findViewById<ImageButton>(R.id.pon)
        val img = findViewById<ImageView>(R.id.imageView)
        val aiko =findViewById<ImageButton>(R.id.aiko)

        pon.setOnClickListener{
            img.setImageResource(R.drawable.stone)
        }

        back.setOnClickListener{
            finish()
        }
        aiko.setOnClickListener{
            finish()
        }
    }
}
