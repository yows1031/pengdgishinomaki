package yosukesugawara.jankensuport

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView

class Paper : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paper)

        val back = findViewById<Button>(R.id.backButtom)
        val pon = findViewById<ImageButton>(R.id.pon)
        val img = findViewById<ImageView>(R.id.imageView)
        val aiko =findViewById<ImageButton>(R.id.aiko)

        pon.setOnClickListener{
            img.setImageResource(R.drawable.pepar)
        }

        back.setOnClickListener{
            finish()
        }
        aiko.setOnClickListener{
            finish()
        }
    }
}
