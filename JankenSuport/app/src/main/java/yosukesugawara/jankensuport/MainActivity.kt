package yosukesugawara.jankensuport

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val stone = findViewById<ImageButton>(R.id.stone)
        val scissors =findViewById<ImageButton>(R.id.scissors)
        val paper = findViewById<ImageButton>(R.id.paper)

        stone.setOnClickListener{
            val intent = Intent(this, Stone::class.java)
            startActivity(intent)
        }

        scissors.setOnClickListener{
            val intent = Intent(this, yosukesugawara.jankensuport.scissors::class.java)
            startActivity(intent)
        }

        paper.setOnClickListener{
            val intent = Intent(this, Paper::class.java)
            startActivity(intent)
        }




    }


}
