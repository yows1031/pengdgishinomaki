package yosukesugawara.jankensuport

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Random : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_random)

        val back = findViewById<Button>(R.id.backButtom)
        back.setOnClickListener{
            finish()
        }
    }
}
