package yosukesugawara.action;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class Droid {

    private static final float GRAVTY = 0.8f;
    private static final float WEIGHT = GRAVTY + 60;

    private static final int BLACK_SIZE = 153;

    private static final Rect BITMAP_SRC_RUNNIING = new Rect(0, 0, BLACK_SIZE, BLACK_SIZE);
    private static final Rect BITMNAP_SRC_JUMPING = new Rect(BLACK_SIZE, 0, BLACK_SIZE * 2, BLACK_SIZE);

    private static final int HIT_MARGIN_LEFT = 30;
    private static final int HIT_MARGIN_RIGHT = 10;

    public interface CallBack{
        int getDistanceFromGround(Droid droid);
    }

    private final CallBack callBack;

    private final Paint paint = new Paint();

    private Bitmap bitmap;

    final RectF rect;
    final Rect hitRect;

    public Droid(Bitmap bitmap, int left, int top, CallBack callBack){
        int right = left + BLACK_SIZE;
        int bottom = top + bitmap.getHeight();
        this.rect = new RectF(left, top, right, bottom);
        this.hitRect = new Rect(left, top, right, bottom);
        this.hitRect.left += HIT_MARGIN_LEFT;
        this.hitRect.right -= HIT_MARGIN_RIGHT;
        this.bitmap = bitmap;
        this.callBack = callBack;
    }

    public void draw(Canvas canvas){
        Rect src = BITMAP_SRC_RUNNIING;
        if(velocity != 0){
            src = BITMNAP_SRC_JUMPING;
        }

        canvas.drawBitmap(bitmap, src, rect, paint);
    }

    private float velocity = 0;

    public void jump(float power){
        velocity  = (power * WEIGHT);
    }

    public void stop(){
        velocity = 0;
    }

    public void move() {

        int distanceFromGround = callBack.getDistanceFromGround(this);

        if (velocity < 0 && velocity < -distanceFromGround) {
            velocity = -distanceFromGround;
        }

        rect.offset(0, Math.round(-1 * velocity));
        hitRect.offset(0, Math.round(-1 * velocity));

        if (distanceFromGround == 0) {
            return;
        } else if (distanceFromGround < 0) {
            rect.offset(0, distanceFromGround);
            hitRect.offset(0, distanceFromGround);
            return;
        }
        velocity -= GRAVTY;

    }
}
