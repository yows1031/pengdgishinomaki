package yosukesugawara.shooting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements GameView.EventCallBak{

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameView = new GameView(this);
        gameView.setEventCallBak(this);
        setContentView(gameView);
    }

    @Override
    public void onGameOver(long score){
        gameView.startDrawThread();
        Toast.makeText(this, "Game Over スコア" + score, Toast.LENGTH_LONG).show();

    }
}
