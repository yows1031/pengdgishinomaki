package yosukesugawara.shooting;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class City extends BaseObject{

    private static final int CITY_HEGHT = 80;
    private final Paint paint = new Paint();
    public final Rect rect;

    public City (int width, int heght){

        int left = 0;
        int top = heght - CITY_HEGHT;
        int right = width;
        int bottom = heght;
        rect = new Rect(left, top, right, bottom);
        yPosition = rect.centerY();
        xPosition = rect.centerX();

        paint.setColor(Color.LTGRAY);
    }

    @Override
    public boolean isHit(BaseObject object){
        if (object.getType() !=Type.Missile){
            return false;
        }

        int x = Math.round(object.xPosition);
        int y = Math.round(object.yPosition);
        return rect.contains(x,y);
    }

    @Override
    public Type getType(){
        return Type.City;
    }

    @Override
    public void draw(Canvas canvas){
        if (state != STATE_NOMMAL){
            return;
        }
        canvas.drawRect(rect,paint);
    }

    @Override
    public void move(){
    }



}
